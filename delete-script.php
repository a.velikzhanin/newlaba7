<?php

include 'db_connect.php';
include 'function.php';
AdminOrNot($db);

$id =$_GET['id'];
$req = "DELETE FROM all_abilities WHERE id= ?";
$res = $db->prepare($req);
$res->execute(array($id));
$req = "DELETE FROM form WHERE id= ?";
$res = $db->prepare($req);
$res->execute(array($id));
$req = "DELETE FROM users WHERE id= ?";
$res = $db->prepare($req);
$res->execute(array($id));


header('Location:admin.php');
