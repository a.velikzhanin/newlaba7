<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();
if (!empty($_SESSION['login']))
    {

        session_destroy();
        if(strip_tags($_COOKIE['admin'])=='1') {
            setcookie('admin','0');
            header('Location:admin.php');
        }
        else {
            header('Location:index.php');
        }

}
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $flag=0;
    ?>

    <html>
    <head>
        <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sign in</title>
        <link  href="style.css" rel="stylesheet"  media="all"/>
    </head>
    <div class="form-container">
        <body>
        <?php
        if (!empty($messages)) {
            print('<div id="messages">');
            // Выводим все сообщения.
            foreach ($messages as $message) {
                print($message);
            }
            print('</div>');
        }
        ?>
        <div class="in-form-container">
            <form action="" accept-charset="UTF-8" method="POST">
                <div class="set">
                    <div class="formname">
                        <label>
                            <?php if ($flag==0) {print '<a><img src="warn.png" alt="Предупреждение"</a>';} ?>
                            Ваш логин
                            <input class="formname" type="text"  name="login" placeholder="Введите логин"
                        </label>
                    </div>
                    <div class="form_mail">
                        <label>
                            <?php if ($flag==0) {print '<a><img src="warn.png" alt="Предупреждение"</a>';} ?>
                            Ваша пароль</label>
                        <input class="formmail" type="password" name="pass" placeholder="Введите пароль">
                    </div>
                </div>
                    <input  style="color:white;margin-left: 100px" type="submit" id="send" class="buttonform" value="Отправить">
                </div>

        </div>
        </form>
    </div>
    </body>
    </div>
    </html>
    <?php
}
else {
    include 'db_connect.php';

    $login=$_POST['login'];
    $stmt = $db->prepare("SELECT * FROM users WHERE login = ?");
    $stmt->execute([$login]);
    $flag=0;
    $id='';
    while($row = $stmt->fetch())
    {
        if(!strcasecmp($row['login'],$_POST['login'])&&password_verify($_POST['pass'],$row['hash']))
        {
            $flag=1;
            $id=$row['id'];
        }
    }
    if($flag) {
        // Если все ок, то авторизуем пользователя.
        $_SESSION['login'] = strip_tags($_POST['login']);
        // Записываем ID пользователя.
        $_SESSION['uid'] = $id;
        // Делаем перенаправление.
        header('Location: index.php');
    }
    else{
        header('Location: login.php');
    }
}
