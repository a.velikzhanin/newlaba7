<?php
header('Content-Type: text/html; charset=UTF-8');
include 'db_connect.php';
include 'function.php';
AdminOrNot($db);

$id =$_GET['id'];
$stmt = $db->prepare("SELECT login FROM users WHERE id = ?");
$stmt->execute(array($id));
$user_login='';
while($row = $stmt->fetch())
{
    $user_login=$row['login'];
}

$request = "SELECT * FROM form where id= ?";
$sth = $db->prepare($request);
$sth->execute(array($id));
$data = $sth->fetch(PDO::FETCH_ASSOC);
if($data==false){
    header('Location:admin.php');
    exit();
}
session_start();
$_SESSION['login'] = $user_login;
// Записываем ID пользователя.
$_SESSION['uid'] = $id;

setcookie('admin','1');
// Делаем перенаправление.
header('Location: index.php');

